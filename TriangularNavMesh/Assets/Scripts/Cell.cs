//寻路网格类
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//网格类型
public enum CellType
{
    Walkable,
    DisWalkable,
}

public class Cell : MonoBehaviour
{
    //网格坐标
    public float posX;
    public float posY;
    public float posZ;

    //网格类型
    public CellType cellType;

    //寻路消耗(f = g + n)
    public float fCount = 0;
    public float gCount = 0;
    public float hCount = 0;

    //是否处于横纵斜方向
    public bool isFirst;

    //父节点
    public Cell fatherCell;

    //颜色
    private MeshRenderer cellRenderer;
    private Color originColor;

    //文本
    private GameObject text;
    private TextMeshPro gText;
    private TextMeshPro fText;
    private TextMeshPro hText;
    private void Awake()
    {
        cellRenderer = transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>();

        text = transform.GetChild(1).gameObject;
        fText = text.transform.GetChild(0).GetComponent<TextMeshPro>();
        gText = text.transform.GetChild(1).GetComponent<TextMeshPro>();
        hText = text.transform.GetChild(2).GetComponent<TextMeshPro>();
        text.SetActive(false);

        originColor = cellRenderer.material.color;
    }

    //初始化
    public void Initialized(float x, float y, float z)
    {
        posX = x;
        posY = y;
        posZ = z;

        cellType = CellType.Walkable;
        fatherCell = null;
    }

    //二维坐标转化成三维坐标
    public Vector3 GetTwoPosFromThreePos(Vector2 pos)
    {
        Vector3 location = Vector3.zero;
        location.x =  - (pos.x/2) + pos.y;
        location.y = pos.x;
        location.z = -location.x - location.y;
        return location;
    }

    //选中网格变色
    public void SelectCell()
    {
        cellRenderer.material.color = new Color(1, 1, 1);
    }

    //重置网格颜色
    public void NoSelectCell()
    {
        cellRenderer.material.color = originColor;
    }

    //作为相邻网格被选中
    public void SelectNeighbours()
    {
        cellRenderer.material.color = new Color(0, 0, 1);
    }

    //作为障碍物被选中
    public void SelectObstruct()
    {
        cellType = CellType.DisWalkable;
        cellRenderer.material.color = new Color(1, 0, 0);
        ResetText();
    }

    //消除障碍物
    public void NoSelectObstruct()
    {
        cellType = CellType.Walkable;
        cellRenderer.material.color = originColor;
    }

    //设置文本
    public void SetText()
    {
        text.SetActive(true);
        fText.text = "F:" + fCount;
        gText.text = "G:" + gCount;
        hText.text = "H:" + hCount;
    }

    public void ResetText()
    {
        text.SetActive(false);
        fText.text = "F:" + 0;
        gText.text = "G:" + 0;
        hText.text = "H:" + 0;
    }

}
