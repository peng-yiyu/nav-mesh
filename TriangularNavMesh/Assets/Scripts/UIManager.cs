//UI管理器类
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    private Text text;
    public static UIManager instance;
    private void Awake()
    {
        text = transform.GetChild(0).GetComponent<Text>();
        instance = this;
    }

    //设置障碍物状态
    public void ObstructState()
    {
        text.text = "当前状态:设置障碍物(按E键切换)";
    }

    //设置起始点状态
    public void SetPiontState()
    {
        text.text = "当前状态:设置起始点(按E键切换)";
    }

}
