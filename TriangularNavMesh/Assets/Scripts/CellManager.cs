//寻路网格管理器类
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CellManager : MonoBehaviour
{
    [Header("寻路网格预制体")]
    public GameObject cellPrefab;
    [Header("寻路地图大小")]
    public int posX;
    public int posY;

    //开启列表
    [SerializeField] private List<Cell> openList = new List<Cell>();
    //关闭列表
    [SerializeField] private List<Cell> closeList = new List<Cell>();
    //寻路网格数组
    private Cell[,] cellGroup;
    //寻路起点
    private Cell startCell;
    //寻路终点
    private Cell endCell;
    //当前目标点
    private Cell curCell;
    //是否已找到终点
    private bool isFind;
    //是否是填充障碍物
    private bool isObstruct;
    //是否需要进行周围全探索
    private bool isAroundFind;
    //是否处于斜线
    private bool isLean;
    //是否处于直线
    private bool isLine;
    //寻找的路径
    private Queue<Cell> cellList = new Queue<Cell>();
    private void Awake()
    {
        cellGroup = new Cell[posX, posY];
        Initialized();    
    }

    private void Update()
    {
        //切换是否是点击修改障碍物
        if(Input.GetKeyDown(KeyCode.E))
        {
            isObstruct = isObstruct == false ? true : false;
            if(isObstruct)
            {
                UIManager.instance.ObstructState();
            }
            else
            {
                UIManager.instance.SetPiontState();
            }
        }

        //选择起始点
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool isCollider = Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("HexCell"));
            if (isCollider)
            {
                //填充障碍物
                if(isObstruct)
                {
                    Cell obstructCell = hit.collider.gameObject.transform.parent.GetComponent<Cell>();
                    if(obstructCell != startCell&&obstructCell!=endCell)
                    {
                        if(obstructCell.cellType == CellType.Walkable)
                        {
                            obstructCell.SelectObstruct();
                        }
                        else
                        {
                            obstructCell.NoSelectObstruct();
                        }
                    }

                    if(startCell!=null&&endCell!=null)
                    {
                        ResetCellColor();
                        startCell.SelectCell();
                        endCell.SelectCell();
                        Cell cell = FindPathing(startCell, endCell);
                        if(cell == null)
                        {
                            Debug.Log("未找到导航路径!!!!");
                        }
                        DrawPathing();
                    }
                }
                //选择起点终点寻路
                else if(hit.collider.gameObject.transform.parent.GetComponent<Cell>().cellType == CellType.Walkable)
                {
                    if (startCell == null)
                    {
                        startCell = hit.collider.gameObject.transform.parent.GetComponent<Cell>();
                        startCell.SelectCell();
                    }
                    else
                    {
                        ResetCellColor();
                        startCell.SelectCell();
                        if (endCell != null)
                        {
                            if (endCell != startCell)
                            {
                                endCell.NoSelectCell();
                            }
                        }
                        endCell = hit.collider.gameObject.transform.parent.GetComponent<Cell>();
                        endCell.SelectCell();

                        Cell cell = FindPathing(startCell, endCell);
                        if (cell == null)
                        {
                            Debug.Log("未找到导航路径!!!!");
                        }
                        DrawPathing();
                    }
                }
            }
        }
    }

    //初始化导航网格地图
    private void Initialized()
    {
        for(int i = 0; i < posX; i++)
        {
            float curZ = i * 0.85f;
            for (int j = 0; j < posY; j++)
            {
                float curX = (float)((i%2 == 0 ?0:0.5)+j);
                GameObject cell = GameObject.Instantiate(cellPrefab,transform);
                cellGroup[i,j] = cell.GetComponent<Cell>();
                cell.GetComponent<Cell>().Initialized(curX, 0, curZ);

                cell.transform.position = new Vector3(curX, 0, curZ);
                cell.transform.rotation = Quaternion.identity;
                cell.transform.localScale = Vector3.one;
            }
        }
    }

    //查找导航路径
    private Cell FindPathing(Cell startCell,Cell endCell)
    {
        openList.Add(startCell);
        curCell = startCell;
        while (openList.Count>0)
        {
            curCell = openList[openList.Count - 1];
            cellList.Enqueue(curCell);
            FindNeighbours(curCell);
            openList.Remove(curCell);
            closeList.Add(curCell);
            if (isFind)
            {
                closeList.Add(endCell);
                return endCell;
            }        
        }

        //碰到死路往回找
        while (cellList.Count > 0)
        {
            openList.Add(cellList.Dequeue());
        }

        while (openList.Count > 0)
        {
            isAroundFind = true;
            curCell = openList[openList.Count - 1];
            FindNeighbours(curCell);
            openList.Remove(curCell);
            closeList.Add(curCell);
            if (isFind)
            {
                closeList.Add(endCell);
                return endCell;
            }
        }

        return null;
    }

    //绘制导航路线
    private void DrawPathing()
    {
        endCell.SelectCell();
        Cell cell = endCell;
        while (cell.fatherCell != null)
        {
            cell.fatherCell.SelectCell();
            cell = cell.fatherCell;
        }
    }

    //查找对应网格的相邻网格（对横纵以及正斜方向进行优先处理）
    private void FindNeighbours(Cell cell)
    {
        Vector2 location = FindCell(cell);
        Vector2 endLocation = FindCell(endCell);
        //起点就是终点
        if (location.x == endLocation.x && location.y == endLocation.y)
        {
            EnterOpenList(cell, (int)location.x, (int)(location.y));
            return;
        }
        //处于斜方位
        if (!isLine&&(((int)(location.x + endLocation.x)%2 == 0&&Mathf.Abs((int)(endLocation.x - location.x)/2) == Mathf.Abs((int)(endLocation.y - location.y)))
            || ((int)location.x % 2 == 0 && (int)(location.x + endLocation.x) % 2 == 1 && ((((int)(endLocation.x - location.x) - 1) / 2 == (int)(endLocation.y - location.y)) || (((int)(endLocation.x - location.x) + 1) / 2 == -(int)(endLocation.y - location.y))))
            || ((int)location.x % 2 == 1 && (int)(location.x + endLocation.x) % 2 == 1 && ((((int)(endLocation.x - location.x) + 1) / 2 == (int)(endLocation.y - location.y)) || (((int)(endLocation.x - location.x) - 1) / 2 == -(int)(endLocation.y - location.y))))))
            {
            Debug.Log("判断为斜线");
            isLean = true;
            cell.isFirst = true;
            //偶数行
            if((int)location.x%2==0)
            {
                //判断是左斜还是右斜
                if((int)location.y <= (int)endLocation.y)
                {
                    if((int)location.x>(int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y-1));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
                        Debug.Log("偶向奇:右下");
                    }
                    else if((int)location.x<(int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y-1));
                        Debug.Log("偶向奇:右上");
                    }         
                }
                else
                {
                    if ((int)location.x > (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y-1));
                        Debug.Log("偶向奇:左下");
                    }
                    else if ((int)location.x < (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y-1));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
                        Debug.Log("偶向奇:左上");
                    }
                }
            }
            else
            {
                //判断是左斜还是右斜
                if ((int)location.y < (int)endLocation.y)
                {
                    if ((int)location.x > (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y + 1));
                        Debug.Log("奇向偶:右下");
                    }
                    else if ((int)location.x < (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y + 1));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y ));
                        Debug.Log("奇向偶:右上");
                    }
                }
                else
                {
                    if ((int)location.x > (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y + 1));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
                        Debug.Log("奇向偶:左下");
                    }
                    else if ((int)location.x < (int)endLocation.x)
                    {
                        EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
                        EnterOpenList(cell, (int)location.x - 1, (int)(location.y + 1));
                        Debug.Log("奇向偶:左上");
                    }
                }
            }

            if (isAroundFind)
            {
                isLean = false;
                isLine = false;
                isAroundFind = false;
                FindCircleNeighbours(cell, location);
            }
        }
        //该位置处于同一行或同一列
        else if (location.x == endLocation.x && !isLean)
        {
            cell.isFirst = true;
            isLine = true;
            EnterOpenList(cell, (int)location.x, (int)(location.y - 1));
            EnterOpenList(cell, (int)location.x, (int)(location.y + 1));
            if (isAroundFind)
            {
                isLean = false;
                isLine = false;
                isAroundFind = false;
                FindCircleNeighbours(cell, location);
            }
        }
        else if (location.y == endLocation.y && !isLean)
        {
            cell.isFirst = true;
            isLine = true;
            EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
            EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
            if (isAroundFind)
            {
                isLean = false;
                isLine = false;
                isAroundFind = false;
                FindCircleNeighbours(cell, location);
            }
        }
        else
        {
            FindCircleNeighbours(cell, location);      
        }
        
        Debug.Log("------------------------");
        //排序
        BestSort();
        for (int i = 0; i < openList.Count; i++)
        {
            Debug.Log("全部成本：" + openList[i].fCount + "剩余估计成本：" + openList[i].hCount + "是否优先：" + openList[i].isFirst);
        }
    }

    //寻找周围网格
    private void FindCircleNeighbours(Cell cell,Vector2 location)
    {
        EnterOpenList(cell, (int)location.x, (int)(location.y - 1));
        EnterOpenList(cell, (int)location.x, (int)(location.y + 1));
        if (location.x % 2 == 0)
        {
            EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
            EnterOpenList(cell, (int)location.x - 1, (int)(location.y - 1));
            EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
            EnterOpenList(cell, (int)location.x + 1, (int)(location.y - 1));
        }
        else
        {
            EnterOpenList(cell, (int)location.x - 1, (int)(location.y));
            EnterOpenList(cell, (int)location.x - 1, (int)(location.y + 1));
            EnterOpenList(cell, (int)location.x + 1, (int)(location.y));
            EnterOpenList(cell, (int)location.x + 1, (int)(location.y + 1));
        }

    }

    //判断是否该加入开启列表
    private void EnterOpenList(Cell cell,int locX,int locY)
    {
        if ((locX >= 0 && locX < posX) && (locY >= 0 && locY < posY))
        {
            Vector2 location = FindCell(cellGroup[locX, locY]);
            Vector2 endLocation = FindCell(endCell);
            if (cellGroup[locX, locY].cellType == CellType.Walkable && !openList.Contains(cellGroup[locX, locY]) && !closeList.Contains(cellGroup[locX, locY]))
            {
                cellGroup[locX, locY].SelectNeighbours();

                cellGroup[locX, locY].gCount = cell.gCount + 1;
                cellGroup[locX, locY].hCount = EndCost(locX, locY);
                cellGroup[locX, locY].fCount = cellGroup[locX, locY].gCount + cellGroup[locX, locY].hCount;

                cellGroup[locX, locY].fatherCell = cell;
                if (cellGroup[locX, locY] == endCell)
                {
                    isFind = true;
                    return;
                }
                //Debug.Log("总成本：" + cellGroup[locX, locY].fCount);
                //Debug.Log("预估成本：" + cellGroup[locX, locY].hCount);
                cellGroup[locX, locY].SetText();
                openList.Add(cellGroup[locX, locY]);

                //判断是否处于斜线和横纵上是就置为优先
                if (location.x == endLocation.x || location.y == endLocation.y || (((int)(location.x + endLocation.x) % 2 == 0 && Mathf.Abs((int)(endLocation.x - location.x) / 2) == Mathf.Abs((int)(endLocation.y - location.y)))
            || ((int)location.x % 2 == 0 && (int)(location.x + endLocation.x) % 2 == 1 && ((((int)(endLocation.x - location.x) - 1) / 2 == (int)(endLocation.y - location.y)) || (((int)(endLocation.x - location.x) + 1) / 2 == -(int)(endLocation.y - location.y))))
            || ((int)location.x % 2 == 1 && (int)(location.x + endLocation.x) % 2 == 1 && ((((int)(endLocation.x - location.x) + 1) / 2 == (int)(endLocation.y - location.y)) || (((int)(endLocation.x - location.x) - 1) / 2 == -(int)(endLocation.y - location.y))))))
                {
                    cellGroup[locX,locY].isFirst = true;
                }
            }
            //遇到阻碍就进行周围搜索
            else if(cellGroup[locX, locY].cellType == CellType.DisWalkable)
            {
                isAroundFind = true;
            }

        }
    }

    //终点预估成本
    private int EndCost(int locX,int locY)
    {
        int distance;
        Vector2 endLocation = FindCell(endCell);
        Vector3 endPos = endCell.GetTwoPosFromThreePos(endLocation);
        Vector2 curLocation = new Vector2(locX, locY);
        Vector3 curPos = cellGroup[locX, locY].GetTwoPosFromThreePos(curLocation);

        distance = (int)((Mathf.Abs(endPos.x - curPos.x) + Mathf.Abs(endPos.y - curPos.y)
            + Mathf.Abs(endPos.z - curPos.z)) / 2);
        
        return distance;

    }

    //开启列表排序()
    private void BestSort()
    {
        //按照FCount大小排序
        for(int i=0;i<openList.Count - 1;i++)
        {
            for(int j = 0; j< openList.Count - 1 - i;j++)
            {          
                if (openList[j].fCount < openList[j + 1].fCount)
                {
                    Swap(j, j + 1);
                }
            }
        }
        //按照HCount大小排序
        for (int i = 0; i < openList.Count - 1; i++)
        {
            for (int j = 0; j < openList.Count - 1 - i; j++)
            {
                if (openList[j].fCount == openList[j + 1].fCount&& openList[j].hCount < openList[j + 1].hCount)
                {
                    Swap(j, j + 1);
                }
            }
        }
        //按照是否处于优先级排序
        for (int i = 0; i < openList.Count - 1; i++)
        {
            for (int j = 0; j < openList.Count - 1 - i; j++)
            {
                if (openList[j].hCount == openList[j + 1].hCount && (openList[j].isFirst == true && openList[j + 1].isFirst == false) )
                {
                    Swap(j, j + 1);
                }
            }
        }
    }

    //查找对应寻路网格坐标
    private Vector2 FindCell(Cell cell)
    {
        for (int i = 0; i < posX; i++)
        {
            for (int j = 0; j < posY; j++)
            {
                if (cell == cellGroup[i,j])
                {
                    return new Vector2(i,j);
                }
            }
        }

        return Vector2.zero;
    }

    //重置网格
    private void ResetCellColor()
    {
        for(int i=0;i<posX;i++)
        {
            for(int j= 0;j<posY;j++)
            {
                if(cellGroup[i,j].cellType == CellType.Walkable)
                {
                    cellGroup[i, j].NoSelectCell();
                    cellGroup[i, j].ResetText();
                    cellGroup[i, j].isFirst = false;
                }
            }
        }

        openList.Clear();
        closeList.Clear();
        cellList.Clear();
        isFind = false;
        isLean = false;
        isLine = false;
    }

    //交换位置
    private void Swap(int i,int j)
    {
        Cell temp = openList[i];
        openList[i] = openList[j];
        openList[j] = temp;
    }
}
